package com.javadev.marathon.ohotina;

import java.util.Scanner;

public class TaskCh04N033 {

    public static void checkLastDigit(int usersDigit) {
        if (usersDigit % 2 == 0) {
            System.out.println("The last digit is even");
        } else {
            System.out.println("The last digit is odd");
        }
    }
}
