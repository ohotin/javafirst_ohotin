package com.javadev.marathon.ohotina;

public class TaskCh03N029 {

    public static boolean checkIfTrueA(int firstNumber, int secondNumber) {
        return (firstNumber % 2 != 0 && secondNumber % 2 != 0);
    }

    public static boolean checkIfTrueB(int firstNumber, int secondNumber) {
        return ((firstNumber > 20 && secondNumber < 20) || (firstNumber < 20 && secondNumber > 20));
    }

    public static boolean checkIfTrueC(int firstNumber, int secondNumber) {
        return (firstNumber == 0 || secondNumber == 0);
    }

    public static boolean checkIfTrueD(int firstNumber, int secondNumber, int thirdNumber) {
        return (firstNumber < 0 && secondNumber < 0 && thirdNumber < 0);
    }

    public static boolean checkIfTrueE(int firstNumber, int secondNumber, int thirdNumber) {
        return  (firstNumber % 5 == 0 && secondNumber % 5 != 0 && thirdNumber % 5 != 0) ||
                (secondNumber % 5 == 0 && firstNumber % 5 != 0 && thirdNumber % 5 != 0) ||
                (thirdNumber % 5 == 0 && firstNumber % 5 != 0 && secondNumber % 5 != 0);
    }

    public static boolean checkIfTrueF(int firstNumber, int secondNumber, int thirdNumber) {
        return (firstNumber > 100 || secondNumber > 100 || thirdNumber > 100);
    }
}

