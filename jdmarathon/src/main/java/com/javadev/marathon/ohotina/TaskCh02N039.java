package com.javadev.marathon.ohotina;

public class TaskCh02N039 {

    public static double getDegrees(int h, int m, int s) {
        if ((h < 0 || h > 23) || (m < 0 || m > 59) || (s < 0 || s > 59)) {
            return -1;
        }

        if (h > 12) {
            h -= 12;
        }

        return h * 30 + m * 0.5 + s * (1.0 / 120);
    }
}