package com.javadev.marathon.ohotina;

public class TaskCh05N064 {
    public static double getDensity(double[][] districts) {
        double averageDensity = 0;

        for (int j = 0; j < 12; j++) {
            averageDensity += districts[0][j] / districts[1][j];
        }

        averageDensity /= 12;

        return averageDensity;
    }
}
