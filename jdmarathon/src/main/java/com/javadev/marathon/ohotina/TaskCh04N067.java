package com.javadev.marathon.ohotina;

public class TaskCh04N067 {

    public static void checkIfWorkdayOrWeekday (int dayToCheck) {

        if (dayToCheck <= 0 || dayToCheck > 365) {
            System.out.println("Invalid number");
        } else if (dayToCheck % 7 <= 5 && dayToCheck % 7 != 0) {
            System.out.println("Workday");
        } else {
            System.out.println("Weekday");
        }
    }
}
