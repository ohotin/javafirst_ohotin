package com.javadev.marathon.ohotina;

public class TaskCh04N115 {

    public static void getNameOfTheYear(int usersYear) {
        int numberToCountTheAnimal = 0;
        String animalOfTheYear, colorOfTheYEar;
        boolean isValidNumber = true;

        if (usersYear < 1) {
            System.out.println("Invalid value");
            isValidNumber = false;
        }

        if (isValidNumber) {
            if (usersYear > 1984) {
                numberToCountTheAnimal = (usersYear - 1984) % 12;
            } else {
                numberToCountTheAnimal = (usersYear - 1984) % 12 + 12;
            }

            switch (numberToCountTheAnimal) {
                case 0:
                    animalOfTheYear = "rat";
                    break;
                case 1:
                    animalOfTheYear = "cow";
                    break;
                case 2:
                    animalOfTheYear = "tiger";
                    break;
                case 3:
                    animalOfTheYear = "rabbit";
                    break;
                case 4:
                    animalOfTheYear = "dragon";
                    break;
                case 5:
                    animalOfTheYear = "snake";
                    break;
                case 6:
                    animalOfTheYear = "horse";
                    break;
                case 7:
                    animalOfTheYear = "sheep";
                    break;
                case 8:
                    animalOfTheYear = "monkey";
                    break;
                case 9:
                    animalOfTheYear = "rooster";
                    break;
                case 10:
                    animalOfTheYear = "dog";
                    break;
                case 11:
                    animalOfTheYear = "pig";
                    break;
                case 12:
                    animalOfTheYear = "rat";
                    break;
                default:
                    animalOfTheYear = "Something is wrong";

            }

            switch (usersYear % 10) {
                case 0:
                case 1:
                    colorOfTheYEar = "white";
                    break;
                case 2:
                case 3:
                    colorOfTheYEar = "blue";
                    break;
                case 4:
                case 5:
                    colorOfTheYEar = "green";
                    break;
                case 6:
                case 7:
                    colorOfTheYEar = "red";
                    break;
                case 8:
                case 9:
                    colorOfTheYEar = "yellow";
                    break;
                default:
                    colorOfTheYEar = "Something is wrong";
            }

            System.out.println("Year " + usersYear + " is year of the " + colorOfTheYEar + " " + animalOfTheYear);
        }
    }
}
