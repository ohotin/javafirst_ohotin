package com.javadev.marathon.ohotina;

import java.util.Scanner;

public class TaskCh02N31 {
    public static void main(String args[]) {
        Scanner usersNumber = new Scanner(System.in);
        System.out.println("Enter a three-digit number");
        int x = usersNumber.nextInt();

        if (x < 100 || x > 999) {
            System.out.println("Invalid number");
        } else {
            int firstDigit, secondDigit, thirdDigit;
            secondDigit = x % 10;
            x /= 10;
            thirdDigit = x % 10;
            firstDigit = x / 10;
            System.out.println(firstDigit * 100 + secondDigit * 10 + thirdDigit);
        }
    }
}
