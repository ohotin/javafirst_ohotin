package com.javadev.marathon.ohotina;

public class TaskCh04N036 {
    public static void getColor(int numberToCheck) {

        if (numberToCheck <= 0) {
            System.out.println("Invalid number");
        } else if (numberToCheck % 5 <= 3 && numberToCheck % 5 != 0) {
            System.out.println("The color is green");
        } else {
            System.out.println("The color is red");
        }
    }
}
