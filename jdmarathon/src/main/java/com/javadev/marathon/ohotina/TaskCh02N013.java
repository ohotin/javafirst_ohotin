package com.javadev.marathon.ohotina;

public class TaskCh02N013 {

    public static int reverseNumber(int numberToReverse) {

        if (numberToReverse < 100 || numberToReverse > 999) {
            return -1;
        }

        int numberToModify = numberToReverse;
        int firstDigit = numberToModify % 10;
        numberToModify /= 10;
        int secondDigit = numberToModify % 10;
        numberToModify /= 10;

        return firstDigit * 100 + secondDigit * 10 + numberToModify;

    }
}
