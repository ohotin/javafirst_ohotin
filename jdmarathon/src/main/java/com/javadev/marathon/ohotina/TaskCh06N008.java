package com.javadev.marathon.ohotina;

public class TaskCh06N008 {

    public static int[] getNumberLessThanInput(int number) {
        int counter = 1;
        while (number > Math.pow(counter, 2)) {
            counter++;
        }

        int[] squares = new int[counter-1];
        for (int j = 0; j < squares.length; j++) {
            squares[j] = (j + 1) * (j + 1);
        }

        return squares;
    }
}
