package com.javadev.marathon.ohotina;

import java.util.Scanner;

public class TaskCh05N010 {

    public static void printTable() {
        Scanner getCurrentDollarRate = new Scanner(System.in);
        System.out.println("Enter current dollar rate:");
        double dollarRate = getCurrentDollarRate.nextDouble();

        if (dollarRate < 0) {
            System.out.println("Dollar rate should be positive");
        } else {
            for (int j = 1; j <= 20; j++ ) {
                System.out.println(j + " $ is equal " + j*dollarRate + " rubles");
            }
        }
    }
}
