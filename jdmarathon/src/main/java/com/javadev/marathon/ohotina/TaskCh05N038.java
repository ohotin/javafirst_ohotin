package com.javadev.marathon.ohotina;

public class TaskCh05N038 {

    public static void getStrangeHusbandPath () {
        double positionAfterMovement = 0;
        double totalPath = 0;

        for (int x = 1; x <= 100; x++) {
            positionAfterMovement += 1.0/x;
            x++;
            positionAfterMovement -= 1.0/x;
        }

        for (int j = 1; j <= 100; j++ ){
            totalPath += 1.0/j;
        }

        System.out.println("The husband will be " + positionAfterMovement + " km far from home");
        System.out.println("Total path made by husband will be equal " + totalPath + " km");
    }
}
