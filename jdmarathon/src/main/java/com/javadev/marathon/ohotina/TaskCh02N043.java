package com.javadev.marathon.ohotina;

public class TaskCh02N043 {
    public static void ifDivide(int numberA, int numberB) {
        int number = ((numberA % numberB == 0) || (numberB % numberA == 0)) ? 1 : 0;
        System.out.println(number);
    }
}
