package com.javadev.marathon.ohotina;

public class TaskCh01N017 {

    public double task01(double x) {
        return Math.sqrt(1 - Math.pow(Math.sin(x), 2));
    }

    public double task02(double a, double b, double c, double x1) {
        return 1 / (Math.sqrt((a * Math.pow(x1, 2) * b * x1 + c)));
    }

    public double task03(double x3) {
        return (Math.sqrt(x3 + 1) + Math.sqrt(x3 - 1)) / (2 * Math.sqrt(x3));
    }

    public double task04(double x4) {

        return Math.abs(x4) + Math.abs(x4 + 1);
    }
}
