package com.javadev.marathon.ohotina;

import java.util.Scanner;

public class TaskCh04N015 {

    public static int calculateAges(int ageOfBirth, int monthOfBirth) {
        Scanner readCurrentYearAndMonth = new Scanner(System.in);

        System.out.println("Please enter current year:");
        int currentYear = readCurrentYearAndMonth.nextInt();

        System.out.println("Please enter current month (1-12):");
        int currentMonth = readCurrentYearAndMonth.nextInt();

        if (currentMonth < 1 || currentMonth > 12) {
            System.out.println("You`ve entered an invalid month");
            return -1;
        }

        int currentAge = (currentYear - ageOfBirth);

        if (currentMonth - monthOfBirth < 0) {
            currentAge--;
        }

        return currentAge;
    }
}
